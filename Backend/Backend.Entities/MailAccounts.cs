﻿namespace Backend.Entities
{
    public class MailAccounts
    {
        public string SendToMail { get; set; }
        public List<SmtpAccount> SmtpAccounts { get; set; }
    }
}
