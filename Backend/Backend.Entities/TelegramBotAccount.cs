﻿namespace Backend.Entities
{
    public class TelegramBotAccount
    {
        public string ApilToken { get; set; }
        public string DebagMessageToUserId { get; set; }
        public string MessageToUserId { get; set; }
    }
}
