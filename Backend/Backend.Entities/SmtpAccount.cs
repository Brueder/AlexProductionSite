﻿namespace Backend.Entities
{
    public class SmtpAccount
    {
        public string Host { get; set; }
        public int Port { get; set; }
        public bool SslEnable { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }
    }
}
