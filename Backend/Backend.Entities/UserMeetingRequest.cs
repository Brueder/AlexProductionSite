﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Backend.Entities
{
    public class UserMeetingRequest
    {
        [DisplayName("Имя")]
        [Required]
        [StringLength(20, MinimumLength = 2)]
        public string Name { get; set; }
        [DisplayName("Телефон")]
        [Required]
        [StringLength(20)]
        public string Telephone { get; set; }
        [DisplayName("Email")]
        [Required]
        [StringLength(40, MinimumLength = 8)]
        public string Email { get; set; }
        [DisplayName("Сообщение")]
        [StringLength(300)]
        public string? Message { get; set; }
    }
}