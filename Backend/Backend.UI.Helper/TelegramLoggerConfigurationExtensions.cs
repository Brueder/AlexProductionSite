﻿using Backend.Entities;
using Backend.Services;
using Microsoft.Extensions.Configuration;
using Serilog;
using Serilog.Configuration;
using Serilog.Core;
using Serilog.Events;

namespace Backend.UI.Helper
{
    public static class TelegramLoggerConfigurationExtensions
    {
        public static LoggerConfiguration Telegram(this LoggerSinkConfiguration sinkConfiguration, LogEventLevel restrictedToMinimumLevel = LogEventLevel.Warning)
        {
            if (sinkConfiguration == null)
            {
                throw new ArgumentNullException("sinkConfiguration");
            }

            return sinkConfiguration.Sink(new TelegramSink());
        }
    }

    internal class TelegramSink : ILogEventSink
    {
        private readonly TelegramService _telegramService;
        public TelegramSink()
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("accounts.json");
            _telegramService = new TelegramService(new Microsoft.Extensions.Logging.Abstractions.NullLogger<TelegramService>(), builder.Build().GetSection("TelegramBot").Get<TelegramBotAccount>(), new UserMeetingRequestValidationService());
        }
        public void Emit(LogEvent logEvent)
        {
            //2022-09-07 20:16:29.268 +00:00 [INF] CORS policy execution successful.
            if (logEvent.Level == LogEventLevel.Error)
            {
                try
                {
                    _telegramService.SendDebugMessageAsync($"{logEvent.Timestamp} [{logEvent.Level}] {logEvent.MessageTemplate}");
                } catch (Exception) { }
            }
        }
    }
}
