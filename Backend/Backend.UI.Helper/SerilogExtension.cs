﻿using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Serilog;
using Serilog.Core;
using Serilog.Events;

namespace Backend.UI.Helper
{
    public static class SerilogExtension
    {
        public static IHostBuilder ConfigureApiSerilogLogging(this IHostBuilder builder) => builder
            .ConfigureLogging((loggingBuilder) =>
            {
                loggingBuilder.ClearProviders();
            })
            .UseSerilog((context, services, configuration) =>
            {
                configuration
                    .MinimumLevel.Information()
                    .WriteTo.Console()
                    .WriteTo.File("Logs/logs.log", rollingInterval: RollingInterval.Day)
                    .WriteTo.Telegram();
            });
    }

}