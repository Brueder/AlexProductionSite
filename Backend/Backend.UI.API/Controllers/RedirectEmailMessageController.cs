﻿using Backend.Entities;
using Backend.Services;
using Backend.Services.Exceptions;
using Microsoft.AspNetCore.Mvc;

namespace Backend.UI.API.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class RedirectEmailMessageController : ControllerBase
    {
        private readonly ILogger<RedirectEmailMessageController> _logger;
        private readonly SmtpService _smtpService;
        private readonly TelegramService _telegramService;
        public RedirectEmailMessageController(ILogger<RedirectEmailMessageController> logger, SmtpService smtpService, TelegramService telegramService)
        {
            _logger = logger;
            _smtpService = smtpService;
            _telegramService = telegramService;
        }

        [HttpPost(Name = "RedirectEmailMessage")]
        public async Task<IActionResult> RedirectEmail(UserMeetingRequest massage)
        {
            bool smtpFlag = true;
            bool telegramFlag = false;

            try 
            { 
                await _telegramService.SendMessageAsync(massage); 
            }
            catch (ValidationServiceException)
            {
                return new StatusCodeResult(409);
            }
            catch (AccountNotFoundException) 
            { 
                telegramFlag = true; 
            }

            try 
            { 
                //await _smtpService.SendEmail(massage); 
            }
            catch (ValidationServiceException)
            {
                return new StatusCodeResult(409);
            }
            catch (AccountNotFoundException) 
            { 
                smtpFlag = true; 
            }
            
            if(smtpFlag && telegramFlag)
            {
                return new StatusCodeResult(503);
            }

            return Ok();
        }
    }
}
