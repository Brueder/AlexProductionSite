﻿using Backend.Entities;
using Backend.Services;

namespace Backend.UI.API
{
    public class Startup
    {
        public IConfiguration Configuration { get; }
        public IConfiguration ConfigurationAccounts { get; }
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;

            var builder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("accounts.json");
            ConfigurationAccounts = builder.Build();
        }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddTransient<SmtpService>();
            services.AddTransient<TelegramService>();
            services.AddTransient<UserMeetingRequestValidationService>();

            services.AddSingleton(new MailAccounts()
            {
                SmtpAccounts = ConfigurationAccounts.GetSection("SmtpAccounts").Get<List<SmtpAccount>>(),
                SendToMail = ConfigurationAccounts.GetSection("SendToEmail").Get<string>()
            });
            services.AddSingleton(ConfigurationAccounts.GetSection("TelegramBot").Get<TelegramBotAccount>());

            services.AddCors(options => options.AddPolicy("CorsPolicy",
                builder =>
                {
                    builder.WithOrigins("http://localhost:3000");
                    builder.WithOrigins("http://localhost");
                }
                ));

            // Add services to the container.
            services.AddControllers();
            // Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
            services.AddEndpointsApiExplorer();
            services.AddSwaggerGen();
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            // Configure the HTTP request pipeline.
            if (env.IsDevelopment())
            {
                app.UseSwagger();
                app.UseSwaggerUI();
            }

            //app.UseHttpsRedirection();

            app.UseRouting();

            app.UseCors("CorsPolicy");

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
