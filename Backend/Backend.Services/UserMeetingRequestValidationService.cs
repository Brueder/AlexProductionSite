﻿using Backend.Entities;
using Backend.Services.Exceptions;
using System.Text.RegularExpressions;

namespace Backend.Services
{
    public class UserMeetingRequestValidationService
    {
        private const string _validationPhone = @"^((8|\+7)[\- ]?)?(\(?\d{3}\)?[\- ]?)?[\d\- ]{7,10}$";
        private const string _validationEmail = @"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$";

        public UserMeetingRequestValidationService(){}

        public void CheckValidation(UserMeetingRequest userMeetingRequest)
        {
            Regex regexEmail = new Regex(_validationEmail);
            Regex regexPhone = new Regex(_validationPhone);

            if(
                string.IsNullOrEmpty(userMeetingRequest.Email) || !regexEmail.IsMatch(userMeetingRequest.Email) ||
                string.IsNullOrEmpty(userMeetingRequest.Telephone) || !regexPhone.IsMatch(userMeetingRequest.Telephone) ||
                string.IsNullOrEmpty(userMeetingRequest.Name) || userMeetingRequest.Name.Length < 2)
            {
                throw new ValidationServiceException($"the object {nameof(UserMeetingRequest)} failed validation");
            }
        }
    }
}
