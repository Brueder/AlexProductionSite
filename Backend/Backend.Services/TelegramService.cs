﻿using Backend.Entities;
using Backend.Services.Exceptions;
using Microsoft.Extensions.Logging;

namespace Backend.Services
{
    public class TelegramService
    {
        private readonly ILogger<TelegramService> _logger;
        private readonly TelegramBotAccount _telegramBotAccount;
        private readonly UserMeetingRequestValidationService _userMeetingRequestValidationService;

        public TelegramService(ILogger<TelegramService> logger, TelegramBotAccount telegramBotAccount, UserMeetingRequestValidationService userMeetingRequestValidationService)
        {
            _logger = logger;
            _telegramBotAccount = telegramBotAccount;
            _userMeetingRequestValidationService = userMeetingRequestValidationService;
        }

        public async Task SendMessageAsync(UserMeetingRequest userMeetingRequest)
        {
            CheckAccount();
            _userMeetingRequestValidationService.CheckValidation(userMeetingRequest);
            string message = $"*Сообщение от клиента:*\n\n {userMeetingRequest.Message}\n\n*Контакты клиента:*\n\n Имя: {userMeetingRequest.Name}\n Телефон: {userMeetingRequest.Telephone}\n Email: {userMeetingRequest.Email}";
            await SendAsync(_telegramBotAccount.MessageToUserId, message);
            _logger.LogInformation("[Telegram] The message was successfully delivered");
        }

        public async Task SendDebugMessageAsync(string message)
        {
            CheckAccount();
            await SendAsync(_telegramBotAccount.DebagMessageToUserId, message);
        }

        private void CheckAccount()
        {
            if (string.IsNullOrEmpty(_telegramBotAccount.ApilToken)) { _logger.LogError("[Telegram] ApilToken not found"); throw new AccountNotFoundException("ApilToken not found");};
            if (string.IsNullOrEmpty(_telegramBotAccount.MessageToUserId)) { _logger.LogError("[Telegram] MessageToUserId not found"); throw new AccountNotFoundException("MessageToUserId not found");};
            if (string.IsNullOrEmpty(_telegramBotAccount.DebagMessageToUserId)) { _logger.LogError("[Telegram] DebagMessageToUserId not found"); throw new AccountNotFoundException("DebagMessageToUserId not found");};
        }

        private async Task<HttpResponseMessage> SendAsync(string userId, string message)
        {
            using (HttpClient client = new HttpClient())
            {
                var content = new FormUrlEncodedContent(new[]
                {
                    new KeyValuePair<string, string>("text", message),
                    new KeyValuePair<string, string>("chat_id", userId),
                    new KeyValuePair<string, string>("parse_mode", "markdown")
                });
                var response = await client.PostAsync($"https://api.telegram.org/bot{_telegramBotAccount.ApilToken}/sendMessage", content);
                return response;
            }
        }
    }
}
