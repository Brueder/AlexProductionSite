﻿using System.Runtime.Serialization;

namespace Backend.Services.Exceptions
{

    [System.Serializable]
    public class ValidationServiceException : Exception
    {
        public ValidationServiceException() { }
        public ValidationServiceException(string message) : base(message) { }
        public ValidationServiceException(string message, Exception innerException) : base(message, innerException) { }
        protected ValidationServiceException(SerializationInfo info, StreamingContext context) : base(info, context) { }
    }
}
