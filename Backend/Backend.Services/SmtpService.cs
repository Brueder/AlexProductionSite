﻿using Backend.Entities;
using Backend.Services.Exceptions;
using MailKit.Net.Smtp;
using Microsoft.Extensions.Logging;
using MimeKit;

namespace Backend.Services
{
    public class SmtpService
    {
        private readonly ILogger<SmtpService> _logger;
        private readonly MailAccounts _mailingAccounts;
        private readonly UserMeetingRequestValidationService _userMeetingRequestValidationService;

        public SmtpService(ILogger<SmtpService> logger, UserMeetingRequestValidationService userMeetingRequestValidationService, MailAccounts mailingAccounts)
        {
            _logger = logger;
            _mailingAccounts = mailingAccounts;
            _userMeetingRequestValidationService = userMeetingRequestValidationService;
        }

        public async Task SendEmail(UserMeetingRequest userMeetingRequest)
        {
            _userMeetingRequestValidationService.CheckValidation(userMeetingRequest);
            if (_mailingAccounts == null)
            {
                _logger.LogError("[SMTP] There is not one mail account");
                throw new AccountNotFoundException("There is not one mail account");
            }

            foreach(var account in _mailingAccounts.SmtpAccounts)
            {
                try
                {
                    var message = CreateMessage(userMeetingRequest, account, _mailingAccounts.SendToMail);
                    Send(account, message);
                    _logger.LogInformation("[SMTP] The message was successfully delivered");
                    break;
                }
                catch (Exception ex)
                {
                    _logger.LogError("[SMTP]" + ex.Message);
                }
            }
        }

        private MimeMessage CreateMessage(UserMeetingRequest userMeetingRequest, SmtpAccount smtpAccount, string ToSend)
        {
            string html = $"<div><h3> Сообщение от клиента:</h3><p style=\"margin-left: 10px\">{userMeetingRequest.Message}</p><h3 style=\"margin-top: 10px\">Контакты клиента:</h3><div style=\"margin-left: 10px\"><p> Имя: {userMeetingRequest.Name}</p><p>Телефон: {userMeetingRequest.Telephone}</p><p>Email: {userMeetingRequest.Email}</p></div><hr/><p style =\"margin-top: 20px\">Это автоматическое сообщение от сайта</p></div>";

            MimeMessage message = new MimeMessage();
            message.From.Add(new MailboxAddress("[Сайт]", smtpAccount.Login));
            message.To.Add(new MailboxAddress("Александр", ToSend));
            message.Subject = $"[Сайт] Запрос консультации от {userMeetingRequest.Name}";
            message.Body = new BodyBuilder() { HtmlBody = html }.ToMessageBody();

            return message;
        }

        private void Send(SmtpAccount smtpAccount, MimeMessage message)
        {
            using (SmtpClient client = new SmtpClient())
            {
                client.Connect(smtpAccount.Host, smtpAccount.Port, smtpAccount.SslEnable);
                client.Authenticate(smtpAccount.Login, smtpAccount.Password);
                client.Send(message);

                client.Disconnect(true);
            }
        }
    }
}