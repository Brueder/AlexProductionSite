# Configs
## Backend
Create an accounts.json file in the Backend/Backend.UI.API directory with submitted content
    
    {
        "SendToEmail": "example@mail.com",
        "SmtpAccounts": [
            {
                "Host": "smtp.mail.com",
                "Port": "465",
                "SslEnable": true,
                "Login": "example@inbox.com",
                "Password": "password123"
            },
        ],
        "TelegramBot" {
            "ApilToken": "0000000000:AAAA-BBBBBBBBBBBBBBBBBBBBB",
            "DebagMessageToUserId": "000000000",
            "MessageToUserId": "000000000"
        }
    }
## Frontend
Create an .env.development file in Frontend/ directory with the submitted content
    
    REACT_APP_RECAPTCHA_KEY=<yourKey>
    REACT_APP_API_URL=api

# How to build
After creating all the configs, simply run the following command
    
    docker-compose up -d

# Components used
## React
- react-roiter-dom
- bootstrap
- react-scripts
- react-youtube
- react-google-recaptcha
- framer-motion
## .Net
- MailKit
- Mictosoft.Extension.Hosting.Abstraction
- Microsoft.VisualStudio.Azure.Conteiners.Tools.Targets
- Serilog
- Serilog.AspNetCore
- Serilog.Extensions.Logging
- Serilog.Setting.Configuration
- Serilog.Sinks.Console
- Serilog.Sinks.File
- Swashbuckle.AspnetCore