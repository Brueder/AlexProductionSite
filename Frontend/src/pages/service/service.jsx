import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import EmailRedirect from '../../api/emailRedirect';

import Treugolnik from '../../components/treugolnik/treugolnik';

import './service.css';
import Photo_Wrapper from "../../resouses/images/service/welcome.png";
import FormPhoto from "../../resouses/images/service/form_img.png";
import Recaptcha from '../../components/recaptcha/recaptcha';

const Service = () => {
    const [windowInnerWidth, setwindowInnerWidth] = useState(0);
    const [isFormCanSubmit, setFormCanSubmit] = useState(true);

    const [telephone, setTelephone] = useState('');
    const [telephoneError, setTelephoneError] = useState('');
    
    const [email, setEmail] = useState('');
    const [emailError, setEmailError] = useState('');
    
    const [name, setName] = useState('');
    const [nameIncorrect, setNameIncorrect] = useState('');

    const [captcha, setCaptcha] = useState(false);
    const [captchaError, setCaptchaError] = useState('');
    
    const [message, setMessage] = useState('');

    const resizeHandler = () => {
      setwindowInnerWidth(document.documentElement.clientWidth);
    };
  
    useEffect(() => {
        (nameIncorrect || emailError || telephoneError) ? (setFormCanSubmit(false)) : (setFormCanSubmit(true)); 
    }, [nameIncorrect, emailError, telephoneError]);

    useEffect(() => {
      window.addEventListener("resize", resizeHandler);
      resizeHandler();
      return () => {
        window.removeEventListener("resize", resizeHandler);
      };
    }, []);
  
    let treugCount = windowInnerWidth / 40;
    
    const nameBlurHandler = (e) => {
        ((21 > e.target.value.length && e.target.value.length > 1) || !e.target.value) ? (setNameIncorrect('')) : (setNameIncorrect('Имя не может быть меньше 2 и больше 20 символов'));
    };

    const telephoneBlurHandler = (e) => {
        let reg = /^((8|\+7)[\- ]?)?(\(?\d{3}\)?[\- ]?)?[\d\- ]{7,10}$/;
        (!reg.test(String(e.target.value).toLowerCase()) && e.target.value) ? (setTelephoneError('Не верный формат ввода телефона')) : (setTelephoneError(''));
    }

    const emailBlurHandler = (e) => {
        let reg = /^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$/;
        (!reg.test(String(e.target.value).toLowerCase()) && e.target.value) ? (setEmailError('Не верный формат ввода эл.почты')) : (setEmailError(''));
    }

    function onChangeCaptcha(value) {
        (value != "" || value != " " || value != null) ? setCaptcha(true) : setCaptcha(false);
    }

    const formHandler = (e) =>{
        e.preventDefault();
        switch (true){
            case !name:
                setNameIncorrect('Имя не может быть пустым');
                return;
            case !telephone:
                setTelephoneError('Телефон не может быть пустым');
                return;
            case !email:
                setEmailError('Эл. почта не может быть пустой');
                return;
            case !captcha:
                alert("Неправельная капча")
                return
        };
        if (isFormCanSubmit){
            const response = EmailRedirect.responseEmailRedirect(name, telephone, email, message);
            response.then(data => {
                switch (data){
                    case 200:
                        setName('');
                        setEmail('');
                        setMessage('');
                        setTelephone('');
                        break;
                    case 503:
                        alert("Ошибка работы сервера, попробуйте повторить заявку через 5 минут...\nИли свяжитесь с нами на прямую по номеру телефона указанного в разделе контакты.");
                        break;
                    default:
                        alert("Неизвестная ошибка, информация уже отправленна разработчику, скоро все починят");
                        break;
                }
            });
        }
    };

    return(
    <div>
        {/* картинка */}
        <div className='w-100 h-25 page_header'>
            <div className="translate-middle text-white" style={{marginTop: "15%", marginLeft: "50%", position: "absolute", zIndex: 2}}>
                <div style={{fontWeight: "bold"}}>Услуги</div>
            </div>
            <div className='w-100 h-100' style={{background: "black", zIndex: 0}}>
                <img className='w-100 h-100' style={{opacity: "0.4"}} src={Photo_Wrapper}></img>
            </div>
        </div>
        {/* основной блок */}
        <div class='service__tariffs__bg'>
            <div class='service__tariffs__wrapper'>
                <div class='service__tariff__card'>
                    <div class='service__tariff__card__head'>
                        <h1>Экономный</h1>
                        <div class='service__tariff__card__line'></div>
                        <h3>₽18000</h3>
                        <h6>3000р/час</h6>
                    </div>
                    <div class='service__tariff__card__content'>
                        <div class='service__tariff__card__sentence'>2-5 часов видеосъемки. Один видеограф</div>
                        <div class='service__tariff__card__sentence'>Входит: Сборы, в загсе, прогулка</div>
                        <div class='service__tariff__card__sentence_special'>Фильм: 20-30 минут</div>
                        <div class='service__tariff__card__sentence'>Клип: 2-4 минут</div>
                        <div class='service__tariff__card__sentence'></div>
                    </div>
                </div>
                <div class='service__tariff__card'>
                    <div class='service__tariff__card__head'>
                        <h1>Премиум</h1>
                        <div class='service__tariff__card__line'></div>
                        <h3>₽45000</h3>
                        <h6>6000р/час</h6>
                    </div>
                    <div class='service__tariff__card__content'>
                        <div class='service__tariff__card__sentence'>15 часов фотосъёмки и видеосъемки в 2 камеры</div>
                        <div class='service__tariff__card__sentence'>Входит: Сборы, в загсе, прогулка, торжество, съемка с коптера</div>
                        <div class='service__tariff__card__sentence'>Фильм: 40-120 минут</div>
                        <div class='service__tariff__card__sentence'>Клип: 2-5 минут</div>
                        <div class='service__tariff__card__sentence'>Фото от 500 штук</div>
                    </div>
                </div>
                <div class='service__tariff__card'>
                    <div class='service__tariff__card__head'>
                        <h1>Оптимальный</h1>
                        <div class='service__tariff__card__line'></div>
                        <h3>₽25000</h3>
                        <h6>4000р/час</h6>
                    </div>
                    <div class='service__tariff__card__content'>
                        <div class='service__tariff__card__sentence'>8 часов фото и видео съемки</div>
                        <div class='service__tariff__card__sentence'>Входит: Сборы, в загсе, прогулка, торжество, съемка с коптера</div>
                        <div class='service__tariff__card__sentence'>Фильм: 20-80 минут</div>
                        <div class='service__tariff__card__sentence'>Клип: 2-5 минут</div>
                        <div class='service__tariff__card__sentence'>Фото от 500 штук</div>
                    </div>
                </div>
            </div>
        </div>
        <Treugolnik 
        count={Math.ceil(treugCount)}/>
        <div class='service__from__bg'>
            <div class='service__form__wrapper'>
                <div class='form__photo'><img src={FormPhoto}></img></div>
                <div class='service__form'>
                    <form onSubmit={formHandler}>
                        <div class='error__message'>{nameIncorrect}</div>
                        <input name='name' type='text' maxLength='20' placeholder='имя' value={name} onChange={(e) => setName(e.target.value)} onBlur={(e) => nameBlurHandler(e)} style={(!nameIncorrect) ?  {color: '#616161'} : {color: 'red'}}/>
                        <div class='error__message'>{telephoneError}</div>
                        <input name='phone' type='tel' maxLength='20' placeholder='телефон' value={telephone} onChange={(e) => setTelephone(e.target.value)} onBlur={(e) => telephoneBlurHandler(e)}/>
                        <div class='error__message'>{emailError}</div>
                        <input name='email' type='email' maxLength='40' placeholder='эл. почта' value={email} onChange={(e) => setEmail(e.target.value)} onBlur={(e) => emailBlurHandler(e)}/>
                        <textarea class='comment__field' name='comment' type='text' maxLength='300' placeholder='комментарий' value={message} onChange={(e) => setMessage(e.target.value)}/>
                        <div className='captcha'><Recaptcha onChange={(value) => onChangeCaptcha(value)}/></div>
                        <input class={(isFormCanSubmit) ? ('submit__btn') : ('unsubmit__btn')} type='submit'/>
                    </form>
                    <div class='conf__politic'>Нажимая на кнопку, вы даете согласие на обработку персональных данных и соглашаетесь с <a><Link to="/privacypolicy">политикаой конфиденциальности</Link></a></div>
                </div>
            </div>
        </div>
    </div>
    );
};

export default Service;