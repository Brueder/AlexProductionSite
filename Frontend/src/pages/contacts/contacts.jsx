import React from 'react';

import "../../styles/app.css"
import "../../styles/tile.css"
import "./contacts.css"

import Photo_Wrapper from "../../resouses/images/contacts/wrapper.png"
import Photo_Contact from  "../../resouses/images/contacts/photo.png"

import Ico_Instagram from "../../resouses/icons/instagram_black.png"
import Ico_Telegram from "../../resouses/icons/telegram_black.png"
import Ico_VK from "../../resouses/icons/vk_black.png"
import Ico_Youtube from "../../resouses/icons/youtube_black.png"
import Ico_Geo from "../../resouses/icons/geo_black.png"


const contacts = () => {

    const phone_number = '+7 (927) 153-81-80';
    const mail = 'aleck.malugin2009@yandex.ru';

    return (
        <div className='tile_conteiner' style={{flexDirection: "column"}}>
        <div className='w-100 h-25 page_header'>
            <div className="translate-middle text-white" style={{marginTop: "15%", marginLeft: "50%", position: "absolute", zIndex: 2}}>
                <div style={{fontWeight: "bold"}}>Контакты</div>
            </div>
            <div className='w-100 h-100' style={{background: "black", zIndex: 0}}>
                <img className='w-100 h-100' style={{opacity: "0.4"}} src={Photo_Wrapper}></img>
            </div>
        </div>
            {/* коробка с контактами */}
            <div class='cotacts__box'>
                {/* блок фото */}
                <div class='cotacts__box__photo__wrapper'>
                    <img src={Photo_Contact}></img>
                    <h3>Александр Малюгин</h3>
                </div>
                {/* блок контактов классических */}
                <div class='classic__contacts'>
                    <div class='classic__contacts__link'><a href='mailto:aleck.malugin2009@yandex.ru'>aleck.malugin2009<br></br>@yandex.ru</a></div>
                    <div class='classic__contacts__link'><a href='tel:+7927153-81-80'>{phone_number}</a></div>
                    <div class='contacts__geo__mark'><img src={Ico_Geo}/>Россия, Саратов</div>
                </div>
                {/* болк с ссылочными контактами */}
                <div class='cotacts__box__links__wrapper'>
                    <div class='cotacts__box__link'><a href='https://t.me/Alex_malyugin'><img src={Ico_Telegram} width="60px" height="60px"></img></a></div>
                    <div class='cotacts__box__link'><a href='https://www.youtube.com/user/yaroslav19741' style={{marginTop: "20%"}}><img src={Ico_Youtube} width="60px" height="60px"></img></a></div>
                    <div class='cotacts__box__link'><a href='https://vk.com/malyugin_videographer'><img src={Ico_VK} width="60px" height="60px"></img></a></div>
                    <div class='cotacts__box__link'><a href='https://www.instagram.com/alex.malyugin_photo/' style={{marginTop: "20%"}}><img src={Ico_Instagram} width="60px" height="60px"></img></a></div>
                </div>
            </div>
            {/* блок обо мне */}
            <div className='about_me_wrapper'>
                <div className='fs-1'>Обо мне</div>
                <div style={{marginLeft: "2%"}}>
                    <div className='fs-3'>Я профессиональный видеограф в Саратове и области. Вхожу в топ-10 свадебных видографов Саратова по версии Горько.</div>
                    <hr/>
                    <div className='fs-3'>Снимаю красивое, стильное, живое и эмоциональное видео, стиль в котором я провожу видеосъемку — это постановочная и репортажная видеосъемка.</div>
                    <hr/>
                    <div className='fs-3'>Провожу бесплатные консультации для своих молодоженов по организации свадьбы и помогаю подобрать лучших специалистов и использую лучшее оборудование.</div>
                </div>
            </div>

        </div>
    );
};

export default contacts;