import React from 'react';
import { Link } from 'react-router-dom';

import "../../styles/tile.css"

const notfound = () => {
    return (
        <div className='tile_conteiner' style={{marginTop: "15%"}}>
            <div>
                <div style={{textAlign: "center", fontSize: "60px", fontWeight: "bold"}}>Ошибка 404</div>
                <div style={{textAlign: "justify", fontSize: "20px", marginTop: "5%"}}>Кажется что-то пошло не так! Страница, которую вы запрашиваете, не существует. <br/> Возможно она устарела, была удалена или был введен неверный адрес в адресной строке</div>
                <div className='tile_conteiner' style={{marginTop: "5%"}}><Link to="/"><button type="button" class="btn btn-secondary fs-4">Перейти на главную</button></Link></div>
            </div>
        </div>
    );
};



export default notfound;