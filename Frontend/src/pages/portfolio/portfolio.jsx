import React from 'react';
import { MPhoto } from '../../components/photo/photo';
import { motion } from 'framer-motion';

import StockVideo from "../../resouses/videos/stock.mp4"
import StockPhoto from "../../resouses/images/image.png"

import YoutubeVideo from '../../components/youtubevideo/youtubevideo'

import "./portfolio.css"
import "../../styles/tile.css"
import "../../styles/app.css"

import Photo_Advantages from "../../resouses/images/portfolio/advantages.png"
import Photo_Drone from "../../resouses/images/portfolio/drone.png"

function importAll(r) {
  return r.keys().map(r);
}

const galleryItemAnimation = {
    hidden:{
        y: 100,
        opacity: 0,
    },
    visible:{
        y: 0,
        opacity: 1,
        transition:{ 
            delay: 0.2
        },
    }
};

const images = [
  "https://i.ibb.co/z7q5FMj/portfolio-0.jpg",
  "https://i.ibb.co/fr6fCbN/portfolio-1.jpg",
  "https://i.ibb.co/k2QbL6B/portfolio-2.jpg",
  "https://i.ibb.co/bP9KQrj/portfolio-3.jpg",
  "https://i.ibb.co/m9JjKxh/portfolio-4.jpg",
  "https://i.ibb.co/P5gmgsG/portfolio-5.jpg",
  "https://i.ibb.co/4Sytjcf/portfolio-6.jpg",
  "https://i.ibb.co/wY7xVtT/portfolio-7.jpg",
  "https://i.ibb.co/z5BcKJk/portfolio-8.jpg",
  "https://i.ibb.co/t8gKs8F/portfolio-9.jpg",
]

function Portfolio() {
    const photoSmallSize = 400;
    const gapSize = 10;
    const photoLargeSize = (photoSmallSize * 2) + gapSize;

  return (
    <div>
      <div class="w-100 vh-100">
        <div class="video-wrapper">
          <video className='w-100 vh-100' src={StockVideo} autoPlay loop muted poster={StockPhoto}></video>
          <div className='overlay'>
            <div style={{textAlight: "center"}} className="position-absolute top-50 start-50 translate-middle text-white text-lg-start">
              <div className='portfolio_title' style={{fontSize: 60, fontWeight: "bold", marginLeft: -2}}>Фото и видео съёмка</div>
              <div className='fs-1' style={{fontWeight: "bold", marginTop: -20}}>В саратове и области</div>
              <div className='fs-5' style={{marginTop: 10}}>Мы умеем снимать трепет души и стук сердец!</div>
            </div>
          </div>
        </div>
      </div>

      <div>
        <div className='background_standart'>
          <div style={{width: "100%", height: 80}}/>
          <div class="treg"></div>
        </div>

        <div className='video_conteiner'>
            <div className='video'><YoutubeVideo videoId="1zNmIVOa9Rc"></YoutubeVideo></div> 
            <div className='video'><YoutubeVideo videoId="M2hx6ykB5bg"></YoutubeVideo></div>
            <div className='video'><YoutubeVideo videoId="JEDazwZo7LA"></YoutubeVideo></div>
            <div className='video'><YoutubeVideo videoId="39eI_It5QD8"></YoutubeVideo></div> 
          {/* <div className='tile_conteiner'>

          </div>
          <div className='tile_conteiner'>
            
          </div> */}
        </div>

        <div 
        className='advantages__wrapper'
        >
          <div className='tile_desc_conteiner'>
            <div className='tile_conteiner'>
              <img className='tile_standart' src={Photo_Advantages} style={{width: 550}}/>
              <div className='tile_standart ' style={{width: 500}}>
                <div className='fs-2'>Профессиональная ретушь</div>
                <div className='fs-5' style={{marginTop: 10}}>Каждый кадр проходит полную цветокоррекцию для максимально правильной передачи цвета.</div>
                <div className='fs-5' style={{marginTop: 10}}>Лучшие кадры подвергаются полному ретушированию для получения снимков высочайшего художественного уровня.</div>
              </div>
            </div>
          </div>
          <div className="tile_desc_conteiner">
            <div className='tile_conteiner' >
            <div className='tile_standart' style={{width: 500}}>
                <div className='fs-2'>Красивые кадры с дрона</div>
                <div className='fs-5' style={{marginTop: 10}}>Детальный и крупный план позволяет делать акценты на чем-то важном в кадре, но пейзажные снимки с высоты – это настоящее искусство. Увеличивая поле зрения через камеру, вы делаете кадр более зрелищным.</div>
              </div>
              <img className='tile_standart' src={Photo_Drone} style={{width: 550}}/>
            </div>
          </div>
        </div>
        
        <motion.div 
        className='photo_cards__wrapper' 
        style={{ columnGap: gapSize }}
        >
            <div className='photo_cards__collom'>
                <MPhoto 
                    initial='hidden'
                    whileInView='visible'
                    img= {images[0]}
                    imgHeight= {photoLargeSize}
                    gapSize= {gapSize}
                    variants= {galleryItemAnimation}
                ></MPhoto>
                <MPhoto 
                    initial='hidden'
                    whileInView='visible'
                    img= {images[1]}
                    imgHeight= {photoSmallSize}
                    gapSize= {gapSize}
                    variants= {galleryItemAnimation}
                />
                <MPhoto
                    initial='hidden'
                    whileInView='visible'
                    img= {images[2]}
                    imgHeight= {photoLargeSize}
                    gapSize= {gapSize}
                    variants= {galleryItemAnimation}
                />
            </div>
            <div className='photo_cards__collom'>
                <MPhoto
                    initial='hidden'
                    whileInView='visible'
                    img= {images[3]}
                    imgHeight= {photoSmallSize}
                    gapSize= {gapSize}
                    variants= {galleryItemAnimation}
                />
                <MPhoto 
                    img= {images[4]}
                    imgHeight= {photoLargeSize}
                    gapSize= {gapSize}
                    variants= {galleryItemAnimation}
                />
                <MPhoto
                    initial='hidden'
                    whileInView='visible' 
                    img= {images[5]}
                    imgHeight= {photoLargeSize}
                    gapSize= {gapSize}
                    variants= {galleryItemAnimation}
                />
            </div>
            <div className='photo_cards__collom'>
                <MPhoto
                    initial='hidden'
                    whileInView='visible' 
                    img= {images[6]}
                    imgHeight= {photoSmallSize}
                    gapSize= {gapSize}
                    variants= {galleryItemAnimation}
                />
                <MPhoto
                    initial='hidden'
                    whileInView='visible' 
                    img= {images[7]}
                    imgHeight= {photoSmallSize}
                    gapSize= {gapSize}
                    variants= {galleryItemAnimation}
                />
                <MPhoto
                    initial='hidden'
                    whileInView='visible' 
                    img= {images[8]}
                    imgHeight= {photoLargeSize}
                    gapSize= {gapSize}
                    variants= {galleryItemAnimation}
                />
                <MPhoto
                    initial='hidden'
                    whileInView='visible' 
                    img= {images[9]}
                    imgHeight= {photoSmallSize}
                    gapSize= {gapSize}
                    variants= {galleryItemAnimation}
                />
            </div>
        </motion.div>
      </div>
    </div>
  );
}

export default Portfolio;