import React from 'react';

export default class EmailRedirect{
    static async responseEmailRedirect(name, telephone, email, message){
        const response = await fetch(process.env.REACT_APP_API_URL, {method: 'POST',   
        headers: {
            'accept': '*/*',
            'Content-Type': 'application/json;charset=utf-8'
         },
          body: JSON.stringify(
            {
                name: name,
                telephone: telephone, 
                email: email, 
                message: message
            }
        )});
        return (response.status);
    }
};