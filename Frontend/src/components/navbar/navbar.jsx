import React, { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';

import "./navbar.css";
import logo from '../../resouses/images/nav/NOVA.svg';
import cMenu from '../../resouses/images/nav/cMenu.svg';
import oMenu from '../../resouses/images/nav/oMenu.svg';

const Navbar = () => {
    const [navbar, setNavbar] = useState(false);
    const [navMenuState, setNavMenuState] = useState(false);
    const [windowInnerWidth, setwindowInnerWidth] = useState(0);

    const resizeHandler = () => {
        setwindowInnerWidth(document.documentElement.clientWidth);
    };

    useEffect(() => {
        if (!navbar && navMenuState) setNavbar(true);
        if (!navMenuState) changeBackground();
    }, [navMenuState]);

    useEffect(() => {
        window.addEventListener("resize", resizeHandler);
        resizeHandler();
        return () => {
          window.removeEventListener("resize", resizeHandler);
        };
    }, []);

    const changeBackground = () => {
        if (window.pageYOffset <= 80 && !navMenuState){
            setNavbar(false);
        } else {
            setNavbar(true);
        }
    }

    window.addEventListener('scroll', changeBackground)

    return(
        <nav className='nav__extender' style={navbar ? {backgroundColor: '#212529'} : {backgroundColor: 'transparent'}}>
            <div className='nav__wrapper'>
                <Link to='/'><img src={logo}></img></Link>
                <div className='nav_links__wrapper' style={navMenuState ? {display: 'flex'} : {display: ''}}>
                    <ul>
                        <li class="nav-item"><Link className="nav_link__item" to="/service" onClick={() => setNavMenuState(false)}>Услуги</Link></li>
                        <li class="nav-item"><Link className="nav_link__item" to="/portfolio" onClick={() => setNavMenuState(false)}>Портфолио</Link></li>
                        <li class="nav-item"><Link className="nav_link__item" to="/contacts" onClick={() => setNavMenuState(false)}>Контакты</Link></li>
                    </ul>
                </div>
                <div className='nav__menu' onClick={() => setNavMenuState(!navMenuState)}><img src={navMenuState ? oMenu : cMenu}></img></div>
            </div>
        </nav>
    );
};

export default Navbar;