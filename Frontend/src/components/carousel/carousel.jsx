import { useEffect, useState, Children, cloneElement } from 'react'
import FaChevronLeft  from "../../resouses/icons/arrow-left.png"
import FaChevronRight from "../../resouses/icons/arrow-right.png"
import './carousel.css'

const PAGE_WIDTH = 480

export const Carousel = ({ children }) => {
  const [pages, setPages] = useState([])
  const [offset, setOffset] = useState(0)

  const handleLeftArrowClick = () => {
    setOffset((currentOffset) => {
      const newOffset = currentOffset + PAGE_WIDTH
      return Math.min(newOffset, 0)
    })
  }
  const handleRightArrowClick = () => {
    setOffset((currentOffset) => {
      const newOffset = currentOffset - PAGE_WIDTH
      const maxOffset = -(PAGE_WIDTH * (pages.length - 1))
      return Math.max(newOffset, maxOffset)
    })
  }

  useEffect(() => {
    setPages(
      Children.map(children, (child) => {
        return cloneElement(child, {
          style: {
            minWidth: `${PAGE_WIDTH}px`,
            maxWidth: `${PAGE_WIDTH}px`,
            height: '100%',
            marginLeft: 10,
          },
        })
      })
    )
  }, [])

  return (
    <div className="main-container">
        <img src={FaChevronLeft} style={{height:50, width:50}} onClick={handleLeftArrowClick}/>
        <div className="window">
            <div
                className="all-pages-container"
                style={{
                    transform: `translateX(${offset}px)`,
                }}
                >
                {pages}
            </div>
        </div>
        <img src={FaChevronRight} style={{height:50, width:50}} onClick={handleRightArrowClick}/>
    </div>
  )
}