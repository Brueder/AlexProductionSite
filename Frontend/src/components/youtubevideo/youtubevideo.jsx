import React from 'react';
import YouTube from 'react-youtube';

const youtubevideo = ({videoId, ...props}) => {
    const opts = {

        playerVars: {
          autoplay: 0,
        },
    };
    const _onReady = (event) => {
        event.target.pauseVideo();
    };

    return (
        <YouTube videoId={videoId} opts={opts} onReady={_onReady}/>
    );
};

export default youtubevideo;