import React, { forwardRef } from 'react';
import { motion } from 'framer-motion';

const photo = forwardRef(({img, imgHeight, gapSize}, ref) => {
    return (
        <div ref={ref} className='photo__item' style={{ width: '100%', overflow: 'hidden'}}>
            <img src={ img } style={{  height: imgHeight, marginBottom: gapSize , width: '100%', objectFit: 'cover', objectPosition: '50% 50%' }}></img>
        </div>
    )
})

export const MPhoto = motion(photo);