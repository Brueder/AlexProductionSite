import React from 'react';

import './treugolnik.css'

const Treugolnik = ({count}) => {
    return (
        <div class='treugolnik__wrapper'>
            {Array(count).fill('').map ((treg, index) => (<div class='treugiknik' key={index}>{treg}</div>))}
        </div>
    );
};

export default Treugolnik;