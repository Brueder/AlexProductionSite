import React from 'react';
import ReCAPTCHA from 'react-google-recaptcha';

const Recaptcha = (props) => {
    const { onChange = {}, ...other } = props; 

    return (
        <ReCAPTCHA
            sitekey={process.env.REACT_APP_RECAPTCHA_KEY}
            onChange={onChange}
            {...other}
        />
    );
};

export default Recaptcha;