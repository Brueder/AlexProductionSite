import React from 'react';
import './footer.css'
import { Link } from 'react-router-dom';
import InstagramICO from '../../resouses/icons/instagram.ico';
import TelegramICO from '../../resouses/icons/telegram.ico';
import VKICO from '../../resouses/icons/vk.ico';

const Footer = () => {
    const phone_number = '+7 (927) 153-81-80';
    const mail = 'aleck.malugin2009@yandex.ru';

    return(
    <div class='footer_wrapper'>
        <footer>
            <div class='footer__content__wrapper'>
                <div class='footer__conten__block'>
                    <div class='footer__title'>Карта сайта</div>
                    <ul role='list' class='footer__map__list'>
                        <li class='footer__map__list__item'><a class='footer__link'><Link to="/service">Услуги</Link></a></li>
                        <li class='footer__map__list__item'><a class='footer__link'><Link to="/portfolio">Портфолио</Link></a></li>
                        <li class='footer__map__list__item'><a class='footer__link'><Link to="/contacts">Контакты</Link></a></li>
                    </ul>
                </div>
                <div class='footer__conten__block'>
                    <div class='footer__title'>Контакты</div>
                    <div class='footer__contacts__block'>
                        <div class='footer__lil__title'>Телефон</div>
                        <a class='footer__contacts__link' href='tel:+7927153-81-80'>{phone_number}</a>
                        <div class='footer__line'></div>
                        <div class='footer__lil__title'>Эл. почта</div>
                        <a class='footer__contacts__link' href='mailto:aleck.malugin2009@yandex.ru'>{mail}</a>
                        <div class='footer__line'></div>
                    </div>
                    <div class='footer__contacts__block'>
                        <div class='footer__lil__title'>Мессенджеры</div>
                        <div >
                            <a href='https://t.me/Alex_malyugin'><img class='footer__contacts__ioc__link' src={TelegramICO}></img></a>
                            <a href='https://vk.com/malyugin_videographer'><img class='footer__contacts__ioc__link' src={VKICO}></img></a>
                            <a href='https://www.instagram.com/alex.malyugin_photo/'><img class='footer__contacts__ioc__link' src={InstagramICO}></img></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class='under__footer__wrapper'>
                <div className='under_footer__contacts'>
                    <a><Link to="/privacypolicy">Политика конфиденциальности</Link></a>
                    <p>Разработанно <a><Link to="/">Itaksoidet studio</Link></a></p>
                </div>
            </div>
        </footer>
    </div>
    )
}

export default Footer;