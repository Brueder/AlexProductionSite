import React from 'react';
import { Route, Routes } from 'react-router-dom';
import Layout from './layout';
import Portfolio from "./pages/portfolio/portfolio"
import Constacts from "./pages/contacts/contacts"
import Service from "./pages/service/service"

import NotFound from "./pages/notfound/notfound"
import PrivacyPolicy from "./pages/privacypolicy/privacypolicy"

const AppRouter = () => {
    return (
        <Routes>
            <Route path='/' element={<Layout/>}>
                <Route path='/' element={<Portfolio/>}/>
                <Route path='/privacypolicy' element={<PrivacyPolicy/>}/>
                <Route path='/portfolio' element={<Portfolio/>}/>
                <Route path='/contacts' element={<Constacts/>}/>
                <Route path='/service' element={<Service/>}/>
            </Route>
            <Route path='*' element={<NotFound/>}/>
        </Routes>
    );
};

export default AppRouter;